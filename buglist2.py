#-*- coding=utf-8 -*-
from threading import _Timer
import  datetime,requests,json
import robot_on_duty

#message_lark  群消息

message1 = "更新最新版case到ies平台"
message2 = "不要忘记写周报"





class LoopTimer(_Timer):
    """Call a function after a specified number of seconds:


            t = LoopTi
            mer(30.0, f, args=[], kwargs={})
            t.start()
            t.cancel()     # stop the timer's action if it's still waiting


    """

    def __init__(self, interval, function, args=[], kwargs={}):
        _Timer.__init__(self, interval, function, args, kwargs)

    def run(self):
        '''self.finished.wait(self.interval)
        if not self.finished.is_set():
            self.function(*self.args, **self.kwargs)
        self.finished.set()'''
        while True:
            self.finished.wait(self.interval)
            if self.finished.is_set():
                self.finished.set()
                break
            self.function(*self.args, **self.kwargs)

def get_week_day(date):
        week_day_dict = {
        0: '星期一',
        1: '星期二',
        2: '星期三',
        3: '星期四',
        4: '星期五',
        5: '星期六',
        6: '星期天',
    }
        day = date.weekday()
        return week_day_dict[day]
bot_token = 'b-c95fbb7e-ff1b-4638-b91a-231103ec57ea'
chat_id = '6552297852532424973'
def robot_on_duty_response(chat_id, bot_token):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}
    oncall_uid_list = robot_on_duty.on_duty(bot_token=bot_token)
    print oncall_uid_list

    # if sys == 'ios':
    #  "open_message_id": "16c83e848eac4403bc41bd1010678c50",
    para = {
          "token":bot_token,
          "chat_id":chat_id,
          "msg_type":"text",
          "mention_user_list":oncall_uid_list,
          "content":{
                "text": "本周邮件反馈和极速反馈值班人员分别为",
                # "title": "反馈跟进"
          }}
    response = requests.post(url=url, data=json.dumps(para))
    print response.json()
def robot_on_duty_response1(chat_id, bot_token):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}
    oncall_uid_list = robot_on_duty.on_duty1(bot_token=bot_token)
    print oncall_uid_list

    # if sys == 'ios':
    #  "open_message_id": "16c83e848eac4403bc41bd1010678c50",
    para = {
          "token":bot_token,
          "chat_id":chat_id,
          "msg_type":"text",
          "mention_user_list":oncall_uid_list,
          "content":{
                "text": "“更新每周长线监控”",
                # "title": "反馈跟进"
          }}
    response = requests.post(url=url, data=json.dumps(para))
    print response.json()
def robot_on_duty_response2(chat_id, bot_token,message):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}
    oncall_uid_list = robot_on_duty.on_duty1(bot_token=bot_token)
    print oncall_uid_list

    # if sys == 'ios':
    #  "open_message_id": "16c83e848eac4403bc41bd1010678c50",
    para = {
          "token":bot_token,
          "chat_id":chat_id,
          "msg_type":"text",
          "content":{
                "text": "{}".format(message),
                # "title": "反馈跟进"
          }}
    response = requests.post(url=url, data=json.dumps(para))
    print response.json()


def send_msg():
    today_week = get_week_day(datetime.datetime.now())
    #print(get_week_day(datetime.datetime.now()))
    now_time = datetime.datetime.now()
    now_hour = str(now_time.strftime('%H'))
    print(now_hour)
    if today_week == "星期一" and now_hour == '11':
        robot_on_duty_response(chat_id, bot_token)
        robot_on_duty_response1(chat_id, bot_token)
    if today_week == "星期一" and now_hour == '15':
        robot_on_duty_response2(chat_id, bot_token,message1)
    if today_week == "星期四" and now_hour == '17':
        robot_on_duty_response2(chat_id, bot_token,message2)
t = LoopTimer(3600, send_msg)
t.start()