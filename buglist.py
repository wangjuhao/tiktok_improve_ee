#coding=utf-8
import requests
import schedule
import time
import datetime
from jira import JIRA
import sys
import pandas as pd
import json
import importlib
import string
# import robot_on_duty.py
importlib.reload(sys)




JIRA_SERVER = 'https://jira.bytedance.com'  # jira地址
JIRA_NAME = 'caixiao'  # 用户名
JIRA_PWD = ''  # 密码
chat_id = ''
Memory_leak = '内存泄漏' #过滤的字段
global edition
edition = 5.5000

def get_user_id_by_email(bot_token, email_prefix):
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/user.user_id'
    data = {
        'token': bot_token,
        'email_prefix': email_prefix,
        'organization_name': 'bytedance'
    }
    client = requests.post(url, data=json.dumps(data))
    resp = json.loads(client.text)
    print(resp)
    user_id = resp['user_id']
    ret = user_id
    return ret
def on_duty(bot_token):
    # return duty people
    oncall_uid_list = []
    # tim = int(time.strftime('%d'))
    # tim = int(time.strftime("%W"))
    # print(tim)
    # today_e = tim % 6
    # today_f = tim % 7
    # print(today_e)

    at_list_email = [
                     {'email': '', 'name': u'王鞠浩'},
                     ]


    oncall_list = []
    for i in range(0,12):
        oncall_list.append(at_list_email[i])
    for oncall_info in oncall_list:
        print(oncall_info)
        # get_user_id_by_email(bot_token=bot_token, email_prefix=item['email'])
        oncall_uid_list.append(int(get_user_id_by_email(bot_token=bot_token, email_prefix=oncall_info['email'])))
    return oncall_uid_list

def robot_on_duty_response(chat_id, bot_token, title, text):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}

    oncall_uid_list = on_duty(bot_token=bot_token)
    para = {
          "token":bot_token,
          "chat_id":chat_id,
          "msg_type":"text",
          "mention_user_list":oncall_uid_list,
          "content":{
                'title': title,
                'text': text
          }}
    print("chat_id: "+chat_id)
    print("bot_token: "+bot_token)
    print(oncall_uid_list)
    requests.post(url=url, data=json.dumps(para))






def get_jira_info(jira_server=JIRA_SERVER,jira_username=JIRA_NAME,jira_password=JIRA_PWD):

    jira = JIRA(basic_auth=(jira_username, jira_password), options = {'server': jira_server})

    #print(jira.user(jira.current_user()))#当前用户

    issue_list = []
    for bug in jira.search_issues('project = AME AND issuetype = Bug AND status in (Open, "In Progress", Reopened) AND affectedVersion = "tiktok '+str(edition)+'" ORDER BY status ASC',maxResults=100):
        #print(bug)
        #print(bug.fields.summary)
        issue_list.append(str(bug.fields.summary.split('\n')))
    print(issue_list)
    issue_list_length = len(issue_list)
    return issue_list_length,issue_list
    # print '新增bug{}个'.format(issue_list_length)
    # print 'buglist 如下：\n'
    # for i in range(0,issue_list_length):
    #     print '{count}、{bug_title}'.format(count=i,bug_title=issue_list[i])

def jira_notice():

    a = 0
    bug_count,bug_list = get_jira_info()
    for i in range(0,bug_count):
        #print(bug_list[i]+'\n')
        if  str(bug_list[i]).find(Memory_leak) != -1:
            continue
        else:
            a += 1
    title =  '有{}个bug待处理,'.format(a)
    text = title + 'bug list 如下：\n'
    a = 0
    for i in range(0,bug_count):
        #print(bug_list[i]+'\n')
        if  str(bug_list[i]).find(Memory_leak) != -1:
            continue
        else:
            a += 1
            text += '{count}、{bug_title}'.format(count=a, bug_title=str(bug_list[i]).replace("[","").replace("'","").replace("]","")) + '\n'
    text += 'bug链接：https://jira.bytedance.com/issues/?jql=project%20%3D%20AME%20AND%20issuetype%20%3D%20Bug%20AND%20status%20in%20(Open%2C%20%22In%20Progress%22%2C%20Reopened)%20AND%20affectedVersion%20%3D%20%22tiktok%20'+str(edition)+'%22%20ORDER%20BY%20summary%20ASC%2C%20status%20ASC'+'\n'

    robot_on_duty_response(chat_id,'b-191e8e19-1fd1-42d7-b530-30ec13dcf5f5', title=title, text=text)

def jira_notice_single():

    if get_week_of_month() % 2 != 0:
        print('single')
    else:
        return

    jira_notice()

def jira_notice_double():
    if get_week_of_month() % 2 == 0:
        print('double')
    else:
        return

    jira_notice()



def edition_change():

    global edition

    edition += 0.1
    edition = round(edition, 2)
    print('edition: ' + str(edition) + '\n')





def get_week_of_month():
    """
    获取指定的某天是某年中的第几周
    周一作为一周的开始
    """
    end = int(datetime.datetime.now().strftime("%W"))
    #begin = int(datetime.datetime(year, month, 1).strftime("%W"))
    #print(datetime.datetime(year, month, day))
    #print(end)
    return end + 1


if __name__ == "__main__":



    print(schedule.__file__)
    jira_notice_single()





    schedule.every().tuesday.at("20:00").do(jira_notice)
    schedule.every().friday.at("20:00").do(jira_notice_double)
    schedule.every().sunday.at("20:00").do(jira_notice_single)

    schedule.every().wednesday.do(edition_change) #升级版本

    #schedule.every()..do(jira_notice)
    while True:
        schedule.run_pending()
        time.sleep(1)
