# encoding=utf-8
import json
import time
import requests
import sys

if sys.getdefaultencoding() != 'utf-8':
    reload(sys)
    sys.setdefaultencoding('utf-8')


# 反馈跟进 Bot Token:b-c95fbb7e-ff1b-4638-b91a-231103ec57ea


def send_lark_grp_msg(bot_token, group_id, title, text, at_list=None):
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # 向群里发消息的post请求
    user_id_list = list()
    # uer_id list 为空
    if at_list:
        # 判断
        for item in at_list:
            # 如果 元素在at_list 列表中
            print(type(item))
            # 打印这个元素
            user_id_list.append({
                'user_id': get_user_id_by_email(bot_token=bot_token, email_prefix=item['email']),
                # user_id 调用get_user_id_by_email
                'name': item['name']
            })
            # user_id_list 空元组添加 user_id，name
    content_arr = [text]
    # content_arr 内容等于[text]
    if user_id_list:
        # 判断
        for item in user_id_list:
            content_arr.append(u'<at user_id="{0}">@{1}</at>'.format(item['user_id'], item['name']))
            # 内容添加user_id  和名字
    data = {
        'token': bot_token,
        'msg_type': 'post',
        'chat_id': group_id,
        'content': {
            'title': title,
            'text': '\n'.join(content_arr)
        }
    }
    headers = {
        'Content-type': 'application/json',
        'Accept': 'text/plain'
    }
    client = requests.post(url, data=json.dumps(data), headers=headers)


def send_lark_user_msg(bot_token, email_prefix, title, text):
    user_id = get_user_id_by_email(bot_token, email_prefix)
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/im.open'
    data = {
        'token': bot_token,  # bot的token
        'user': user_id
    }
    client = requests.post(url, data=json.dumps(data))
    resp = json.loads(client.text)
    channel_id = resp['channel']['id']
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    data = {
        'token': bot_token,
        'msg_type': 'post',
        'chat_id': channel_id,
        'content': {
            'title': title,
            'text': text
        }
    }
    headers = {
        'Content-type': 'application/json',
        'Accept': 'text/plain'
    }
    requests.post(url, data=json.dumps(data), headers=headers)


def get_user_id_by_email(bot_token, email_prefix):
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/user.user_id'
    data = {
        'token': bot_token,
        'email_prefix': email_prefix,
        'organization_name': 'bytedance'
    }
    client = requests.post(url, data=json.dumps(data))
    resp = json.loads(client.text)
    user_id = resp['user_id']
    ret = user_id
    return ret


def get_bot_token(user_token, email):
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/bot.create'
    data = {
        'token': user_token,
        'email_prefix': email,
        'organization_name': 'bytedance'
    }

    client = requests.post(url, json.dumps(data))
    resp = json.loads(client.text)
    return resp


def update_bot_info(user_token, bot_token, bot_name, avatar_key):
    url = "https://oapi.zjurl.cn/open-apis/api/v2/bot/update"
    data = {
        'token': user_token,
        'bot': bot_token,
        'name': bot_name,
        'avatar_key': avatar_key
    }

    client = requests.post(url, json.dumps(data))
    resp = json.loads(client.text)
    return resp


def invite_bot_to_group(user_token, bot_token, group_id):
    url = "https://oapi.zjurl.cn/open-apis/api/v2/bot/chat/join"
    data = {
        'token': user_token,
        'bot': bot_token,
        'group_id': group_id
    }

    client = requests.post(url, json.dumps(data))
    resp = json.loads(client.text)
    return resp


def get_bot_info(user_token, bot_token):
    url = "https://oapi.zjurl.cn/open-apis/api/v1/bot.info"
    data = {
        'token': user_token,
        'bot': bot_token
    }

    client = requests.post(url, json.dumps(data))
    resp = json.loads(client.text)
    return resp


def upload_image(image_path, bot_token):
    file = None
    with open(image_path, 'rb') as f:
        file = f.read()

    r = requests.post(
        url='https://oapi.zjurl.cn/open-apis/api/v2/message/image/upload',
        files={
            'image': file  # 这里是二进制流
        },
        data={
            'token': bot_token,
        }
    )
    return r.json()


def run(bot, group_id, title, content, at_list):
    send_lark_grp_msg(bot, group_id, title, content, at_list)


# -------端监控值班机器人-----------
# return 值班人uid
def on_duty(bot_token):
    # return duty people
    oncall_uid_list = []
    # tim = int(time.strftime('%d'))
    tim = int(time.strftime("%W"))
    print(tim)
    today_e = tim % 6
    today_f = tim % 7
    print(today_e)

    at_list_email = [{'email': 'wangyixuan.cici', 'name': u'王倚暄'},
                     {'email': 'pengjiajun', 'name': u'彭嘉君'},
                     {'email': 'weiyuhang', 'name': u'魏宇航'},
                     {'email': 'wangzhiwei.01', 'name': u'王志伟'},
                     {'email': 'xingtianlong', 'name': u'邢天龙'},
                     {'email': 'zhanglei.1996', 'name': u'张磊'}
                     ]
    at_list_fast = [{'email': 'lichangqiang', 'name': u'李长强'},
                    {'email': 'zhanglei.1996', 'name': u'张磊'},
                    {'email': 'wangyixuan.cici', 'name': u'王倚暄'},
                    {'email': 'xingtianlong', 'name': u'邢天龙'},
                    {'email': 'pengjiajun', 'name': u'彭嘉君'},
                    {'email': 'weiyuhang', 'name': u'魏宇航'},
                    {'email': 'wangzhiwei.01', 'name': u'王志伟'}
                    ]

    oncall_list = []
    oncall_list.append(at_list_fast[today_f])
    oncall_list.append(at_list_email[today_e])
    for oncall_info in oncall_list:
        print(oncall_info)
        # get_user_id_by_email(bot_token=bot_token, email_prefix=item['email'])
        oncall_uid_list.append(int(get_user_id_by_email(bot_token=bot_token, email_prefix=oncall_info['email'])))
    return oncall_uid_list


# @-----------菜潇反馈----------------------
def on_duty1(bot_token):
    # return duty people
    oncall_uid_list = []
    # tim = int(time.strftime('%d'))
    tim = int(time.strftime("%W"))
    print(tim)
    today_e = tim % 6
    today_f = tim % 7
    print(today_e)

    oncall_list = [{'email': 'caixiao', 'name': u'蔡潇'}]

    for oncall_info in oncall_list:
        print(oncall_info)
        # get_user_id_by_email(bot_token=bot_token, email_prefix=item['email'])
        oncall_uid_list.append(int(get_user_id_by_email(bot_token=bot_token, email_prefix=oncall_info['email'])))
    return oncall_uid_list


def on_duty_fast(bot_token):
    # return duty people
    oncall_uid_list = []
    # tim = int(time.strftime('%d'))
    tim = int(time.strftime("%W"))
    print(tim)
    today_e = tim % 6
    today_f = tim % 7
    print(today_e)

    at_list_fast = [{'email': 'lichangqiang', 'name': u'李长强'},
                    {'email': 'zhanglei.1996', 'name': u'张磊'},
                    {'email': 'wangyixuan.cici', 'name': u'王倚暄'},
                    {'email': 'xingtianlong', 'name': u'邢天龙'},
                    {'email': 'pengjiajun', 'name': u'彭嘉君'},
                    {'email': 'weiyuhang', 'name': u'魏宇航'},
                    {'email': 'wangzhiwei.01', 'name': u'王志伟'}
                    ]

    fast_uid_list = []
    fast_uid_list.append(at_list_fast[today_f])
    for oncall_info in fast_uid_list:
        print
        oncall_info
        # get_user_id_by_email(bot_token=bot_token, email_prefix=item['email'])
        oncall_uid_list.append(int(get_user_id_by_email(bot_token=bot_token, email_prefix=oncall_info['email'])))
    return fast_uid_list


# 回复@消息
def robot_on_duty_response(root_id, chat_id, bot_token, key_word):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}
    oncall_uid_list = on_duty(bot_token=bot_token)
    print
    oncall_uid_list

    if key_word == 'duty':
        para = {
            "token": bot_token,
            "chat_id": chat_id,
            "msg_type": "text",
            "open_message_id": "16c83e848eac4403bc41bd1010678c50",
            "root_id": root_id,
            "mention_user_list": oncall_uid_list,
            "content": {
                "text": "本周极速反馈和邮件反馈值班人是",
                # "title": "反馈跟进"
            }}
    elif key_word == 'list':
        para = {
            "token": bot_token,
            "chat_id": chat_id,
            "msg_type": "text",
            "open_message_id": "16c83e848eac4403bc41bd1010678c50",
            "root_id": root_id,
            "content": {
                "text": "极速反馈值班人员:\n" + "1.李长强\n2.张磊\n3.王倚暄\n4.邢天龙\n5.彭嘉君\n6.魏宇航\n7.王志伟\n邮件反馈值班人员:\n1.王倚暄\n"
                        + "2.彭嘉君\n3.魏宇航\n4.王志伟\n5.邢天龙\n6.张磊\n"
                # "title": "反馈跟进"
            }}
    response = requests.post(url=url, data=json.dumps(para))
    print
    response.json()


