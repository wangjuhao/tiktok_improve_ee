#coding=utf-8
import requests
import schedule
import time
import datetime
from jira import JIRA
import sys
import pandas as pd
import json
import importlib
import string
import http.client
import base64
import gzip
import re
# import robot_on_duty.py
importlib.reload(sys)




docToken = 'doccnNmXUF3Gk3miL1S82t'
bot_token = 'b-191e8e19-1fd1-42d7-b530-30ec13dcf5f5'
title = '温馨提示：请您发下版本发版/提审邮件'
email_prefix = 'wangjuhao'
chat_id = '6669280087071785219'
def doc_con_api_key():
    #conn = http.client.HTTPSConnection("oapi.zjurl.cn")
    basicAuth = base64.b64encode(str.encode('lMg5ziXRonF72vyo0LCLYYaSUdotHuzas47c0JD5PdNZWqBJ74MZ1pWtsbGOUXKM' + ':'))

    headers = \
        {
        "authorization": "Basic " + bytes.decode(basicAuth),
        "Content-Type": "application/json"

        }
    body = \
        {
            "type":"doc" or "sheet",
            "token":"doccnNmXUF3Gk3miL1S82t"
        }
    print(body)
    #response = requests.post("https://oapi.zjurl.cn/associates/v1/associatefile", headers=headers,data=body)
    response = requests.post(url = "https://oapi.zjurl.cn/associates/v1/associatefile", headers=headers, data=body)
    print(response.text)
# conn.request("POST", "https://oapi.zjurl.cn/associates/v1/associatefile",  data =body, headers=headers )
    # res = conn.getresponse()
    # data = res.read()
    #
    # print(data.decode("utf-8"))

def read_in_docs(docToken):


    conn = http.client.HTTPSConnection("oapi.zjurl.cn")
    basicAuth = base64.b64encode(str.encode('lMg5ziXRonF72vyo0LCLYYaSUdotHuzas47c0JD5PdNZWqBJ74MZ1pWtsbGOUXKM' + ':'))

    headers = {
        "authorization": 'Basic ' + bytes.decode(basicAuth),
        "Accept-Encoding": 'gzip'
    }

    conn.request("GET", "https://oapi.zjurl.cn/docs/v1/" + docToken + "/content", headers=headers)

    res = conn.getresponse()
    data = res.read()
    ret = gzip.decompress(data).decode("utf-8")
    return ret


def find_new_demand_list(all_list):
    pattern = re.compile(r'([0-9]{3}\u7248\u672c\u9700\u6c42\u5982\u4e0b)')#xxx版本需求如下
    arry = pattern.findall(all_list)
    print(arry)
    index1 = all_list.index(arry[0])
    index2 = all_list.index(arry[1])

    new_list = all_list[index1:index2]
    new_list = new_list.replace("*","")
    return new_list

def on_duty(bot_token):
    # return duty people
    oncall_uid_list = []
    # tim = int(time.strftime('%d'))
    # tim = int(time.strftime("%W"))
    # print(tim)
    # today_e = tim % 6
    # today_f = tim % 7
    # print(today_e)

    at_list_email = [{'email': 'gutianyu', 'name': u'谷天宇'},
                     {'email': 'wangjuhao', 'name': u'王鞠浩'},
                     {'email': 'caixiao', 'name': u'蔡潇'}
                    ]


    oncall_list = []
    for i in range(0,3):
        oncall_list.append(at_list_email[i])
    for oncall_info in oncall_list:
        print(oncall_info)
        # get_user_id_by_email(bot_token=bot_token, email_prefix=item['email'])
        oncall_uid_list.append(int(get_user_id_by_email(bot_token=bot_token, email_prefix=oncall_info['email'])))
    return oncall_uid_list

def get_user_id_by_email(bot_token, email_prefix):
    print(bot_token)
    print(email_prefix)
    url = 'https://oapi.zjurl.cn/open-apis/api/v1/user.user_id'
    data = {
        "token": bot_token,
        "email_prefix": email_prefix,
        "organization_name": "bytedance"
    }
    client = requests.post(url, data=json.dumps(data))
    resp = json.loads(client.text)
    print(resp)
    user_id = resp['user_id']
    ret = user_id
    return ret


def robot_on_duty_response(chat_id, bot_token, title, text):
    # root_id要回复消息的id
    # bot_token 机器人token
    url = 'https://oapi.zjurl.cn/open-apis/api/v2/message'
    # head = {'content-type': 'application/json'}

    oncall_uid_list = on_duty(bot_token=bot_token)
    para = {
          "token":bot_token,
          "chat_id":chat_id,
          "msg_type":"text",
          "mention_user_list":oncall_uid_list,
          "content":{
                'title': title,
                'text': text
          }}
    print("chat_id: "+chat_id)
    print("bot_token: "+bot_token)
    print(oncall_uid_list)
    requests.post(url=url, data=json.dumps(para))

def running():
    all_list = read_in_docs(docToken)
    new_list = find_new_demand_list(all_list)
    print(new_list)
    robot_on_duty_response(chat_id, bot_token, title, new_list)

if __name__ == "__main__":
    print(schedule.__file__)
    running()
    schedule.every().monday.at("11:00").do(running)


    #schedule.every()..do(jira_notice)
    while True:
        schedule.run_pending()
        time.sleep(1)