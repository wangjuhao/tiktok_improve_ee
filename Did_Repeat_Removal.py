# -*- coding: utf-8 -*-

import sys
import xlrd
import xlwt

import os
#打开excel文件
path_mother = '/Users/wangjuhao/mother_3.xlsx'
path_son = '/Users/wangjuhao/sun_3.xlsx'
path_mother_wr = '/Users/wangjuhao/finish_4'
def open_excel(file):
        data = xlrd.open_workbook(file)
        print("success")
        print(data)
        return data


#根据名称获取Excel表格中的数据   参数:file：Excel文件路径     colnameindex：表头列名所在行的索引  ，by_name：Sheet1名称
def excel_table_byname(file, colnameindex, by_name):
    data = open_excel(file) #打开excel文件
    table = data.sheet_by_name(by_name) #根据sheet名字来获取excel中的sheet
    nrows = table.nrows #行数
    colnames = table.row_values(colnameindex) #某一行数据
    list =[] #装读取结果的序列
    for rownum in range(0, nrows): #遍历每一行的内容
         row = table.row_values(rownum) #根据行号获取行
         if row: #如果行存在
             app = [] #一行的内容
             for i in range(len(colnames)): #一列列地读取行的内容
                app.append(row[i])
             list.append(app) #装载数据
    return list

# def read03Excel(path):
#     workbook = xlrd.open_workbook(path)
#     sheets = workbook.sheet_names()
#     print(sheets)
#     worksheet = workbook.sheet_by_name(sheets[0])
#     for i in range(0, worksheet.nrows):
#         row = worksheet.row(i)
#         for j in range(0, worksheet.ncols):
#             print(worksheet.cell_value(i, j), "\t", end="")
#         print()
def write_file(finish, path):
    f = open(path,'w')
    #for line in lists:
        #print(type(line))
    f.write(finish.replace("'",""))
    print(1)

def repeat_remove(l1, l2):
    l3 = [x for x in l1 if x not in l2]
    l4 = str(l3).replace("[", "").replace(",", "").replace("]", "").replace(" ", "\n")
    #print(l4)
    return l4

#主函数
def main():
    tables1 = excel_table_byname(path_mother,0,u'Sheet1')
    tables2 = excel_table_byname(path_son,0,u'Sheet1')
    finish = repeat_remove(tables1, tables2)
    write_file(finish, path_mother_wr)



if __name__=="__main__":
    main()
